# Plots #

Different plots and graphs

* `freq_compare`
  - multiple data lines from CSV files (color)
  - log x-axis (base 2)
  - log y-axis (base 10)
* `mem_compare`
  - two data lines from CSV files (black & white)
  - log x-axis (base 2)
  - linear y-axis
* `tech_compare`
  - two figures - suitable for combining into a subfigure
  - multiple data lines from CSV files (color)
  - linear x-axis
  - linear y-axis
* `stacked_bars`
  - two stacked bars plots next to each other
  - additional custom labels under one of them
